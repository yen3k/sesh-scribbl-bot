#!/usr/bin/python
# coding: utf-8

from telegram.ext import Updater, MessageHandler, Filters
import os.path

global token

def update_callback(update, context):

    message = update.message.text

    if (len(message) > 30):
      update.message.reply_text("Et ord maa maks vaere 30 bogstaver langt")
      return

    if (not all(x.isalpha() or x.isdigit() or x.isspace() for x in message)):
        update.message.reply_text("Ord maa kun indeholde bogstaver og tal")
        return

    cont = True
    with open("SAVED", "r") as f:
        for line in f:
            print("test1")
            if message == line:
                cont = False
                break
    if cont:
        with open("SAVED", "a") as f:
            f.write(message + "\n")

    update.message.reply_text(message + " er gemt!")

def test_callback(update, context):
    print("message")

def main():
    with open("TOKEN", "r") as f:
        token = f.readline().strip()

    updater = Updater(token, use_context=True)

    updater.dispatcher.add_handler(MessageHandler(callback=update_callback,
                                                  filters=Filters.text))

    updater.start_polling()
    updater.idle()

def check_chat_id(chatid):
    with open("CHATID") as f:
        realchatid = f.readline().strip()
    return realchatid == chatid

if __name__ == '__main__':
    if not os.path.isfile('SAVED'):
        open('SAVED', 'a').close()
    main()
